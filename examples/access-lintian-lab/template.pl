# Sample hook/template script...

# The (extra) arguments are available via @ARGV
#
if (@ARGV) {
    print "Arguments:\n";
    foreach my $arg (@ARGV) {
        print "    $arg\n";
    }
    print "End of arguments:\n";
}


## setup/tear down hooks

# Called just after the lab has been opened, but before any visit
# hooks.  Its only argument is the instance of Lintian::Lab.
sub lab_open {
    my ($lab) = @_;
    print "Lab opened\n";
}

# Called after the last visit (if any).  It is always given an
# instance of the lab as first argument.  If any of the hooks
# (including lab_open) threw a trappable error (e.g. invoked die),
# the trapped error is given as second argument.
#
# This is the last hook to be called by access-lintian-lab.  The lab
# is still open at this point and will be closed by the caller.
sub lab_close {
    my ($lab, $error) = @_;
    print "Lab about to be closed\n";
}

## visit hooks
#
# All entries in the lab are passed to one of the visit_<type> hook
# depending on the type of the entry.  Example, visit_binary is
# invoked once for every binary (.deb) package in the Laboratory.
#
# visit is invoked once for each entry the lab regardless of its type.
# The order between visit and visit_<type> is unspecified and
# considered an implementation detail of access-lintian-lab.
#
# The order in which entries are visited depends on Lintian::Lab and
# is not considered a part of access-lintian-lab's API.
#
# The visit hooks are given exactly one argument, which is an instance
# of Lintian::Lab::Entry.
#

sub visit {
    my ($entry) = @_;
    print 'visit (' , $entry->identifier, ")\n";
}

sub visit_binary {
    my ($entry) = @_;
    print 'visit_binary (' , $entry->identifier, ")\n";
}

sub visit_source {
    my ($entry) = @_;
    print 'visit_source (' , $entry->identifier, ")\n";
}

sub visit_udeb {
    my ($entry) = @_;
    print 'visit_udeb (' , $entry->identifier, ")\n";
}

1;
